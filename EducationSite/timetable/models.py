from django.db import models
import datetime,time
from django.contrib.auth.models import User
import locale
from django.db.models.signals import post_save
from django.dispatch import receiver
locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')


class Faculty(models.Model):
    name = models.CharField(help_text="Название Факультета", max_length=100)
    short_name = models.CharField(help_text="Короткое название факультета", max_length=50)
    dean = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.short_name


class Group(models.Model):
    name = models.CharField(max_length=150, help_text="Полное название группы")
    short_name = models.CharField(max_length=50, help_text="Сокращенное название группы")
    num = models.IntegerField(help_text="Уникальный номер группы")
    faculty = models.ForeignKey(Faculty, on_delete=models.CASCADE)

    def __str__(self):
        return self.short_name + str(self.num)

    def get_students(self):
        students = self.profile_set.all()
        return students


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, blank=True, null=True)

    def marks_and_skips(self):
        lessons = Lesson.objects.filter(group=self.group)

    def __str__(self):
        return self.user.username


class Lesson(models.Model):
    Type_of_Event = [('лекция', 'лекция'),
                     ('практика', 'практика'),
                     ('экзамен', 'экзамен')]
    name = models.CharField(max_length=50,help_text="Name of event")
    type = models.CharField(max_length=50,help_text="lecture,practice,exam", choices=Type_of_Event)
    teacher = models.ForeignKey(Profile,on_delete=models.CASCADE,  blank=True, null=True)
    date = models.DateField(help_text="time of event")
    time_start = models.TimeField(help_text="start of event")
    time_end = models.TimeField(help_text="end of event")
    room = models.CharField(max_length=50,help_text="place of event")
    theme = models.CharField(max_length=150,help_text="theme of event", blank=True)
    dist = models.CharField(max_length=150,help_text="link to online resources", blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)

    class Meta:
        permissions = (("can_edit_lesson", "Edit dist,theme of lesson"), )

    def __str__(self):
        return self.name

    def get_month(self):
        return self.date.strftime('%B')

    @property
    def week(self):
        return self.date.isocalendar()[1]


class Mark(models.Model):
    mark = models.IntegerField( blank=True, null=True)
    skip = models.BooleanField( default=False)
    person = models.ForeignKey(Profile, on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.lesson.date) + "_" + self.lesson.name + "_" + self.person.user.username

