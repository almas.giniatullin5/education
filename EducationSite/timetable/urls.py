from django.urls import path
from . import views
from django.conf.urls import url
from django.urls import path
from django.contrib.auth.views import LogoutView


urlpatterns = [
    url(r'^$', views.index, name='index'),
    path('lesson/<int:pk>/edit', views.edit_lesson, name='edit_lesson'),
    path('lesson/<int:pk>/active', views.active_lesson, name='active_lesson'),
    path('lesson/<int:pk>/', views.lesson_detail, name='lesson_detail'),
    path('group/<int:pk>', views.group_detail, name='group_detail'),
    path('activity/<int:pk>', views.export_activity_to_xlsx, name='export_activity_to_xlsx')
]


