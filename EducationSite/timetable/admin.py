from django.contrib import admin
from .models import Profile,Lesson, Group, Mark, Faculty
# Register your models here.
admin.site.register(Group)
admin.site.register(Profile)
admin.site.register(Lesson)
admin.site.register(Mark)
admin.site.register(Faculty)
