from django import forms
from .models import Lesson, Mark


class EditLessonForm(forms.ModelForm):

    class Meta:
        model = Lesson
        fields = ('dist', 'theme')
        labels = {
            'dist': ('Ссылка на ресурс'),
            'theme': ('Тема'),
        }
        help_texts = {
            'dist': (''),
            'theme': (''),
        }


class StudentActivityForm(forms.ModelForm):

    class Meta:
        model = Mark
        fields = ('person', 'mark', 'skip')
        labels = {
            'person': ('Студент'),
            'mark': ('Оценка'),
            'skip': ('Пропуск'),
        }



