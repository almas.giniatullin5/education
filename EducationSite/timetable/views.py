from django.shortcuts import render
from .models import Lesson, Profile, Group, Faculty, Mark
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.decorators import login_required
from .forms import EditLessonForm, StudentActivityForm
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from openpyxl import Workbook
from django.http import HttpResponse
from datetime import datetime
from django.db.models.functions import TruncWeek
import logging
from django.forms import formset_factory

logger = logging.getLogger(__name__)


@login_required
def index(request):
    user = request.user
    profile = user.profile
    if user.groups.filter(name="Деканы").exists():
        faculty = get_object_or_404(Faculty, dean=user)
        groups = faculty.group_set.all()
        return render(request, 'dashboard_dean.html', context={'faculty': faculty, 'groups': groups})
    elif user.groups.filter(name="Диспетчеры").exists():
        return render(request, 'dashboard_admin.html')
    else:
        if user.groups.filter(name="Преподаватели").exists():
            lessons = Lesson.objects.filter(teacher=profile)
        else:
            lessons = Lesson.objects.filter(group=profile.group).order_by()
            logger.error(lessons)
        return render(request, 'dashboard.html', context={'lessons': lessons})


@login_required
def lesson_detail(request, pk):
    lesson = get_object_or_404(Lesson, pk=pk)
    return render(request, 'lesson_detail.html', {'lesson': lesson})


@permission_required('timetable.can_edit_lesson')
def edit_lesson(request, pk):
    lesson = get_object_or_404(Lesson, pk=pk)
    # Если данный запрос типа POST, тогда
    if request.method == 'POST':

        # Создаём экземпляр формы и заполняем данными из запроса (связывание, binding):
        form = EditLessonForm(request.POST, instance=lesson)

        # Проверка валидности данных формы:
        if form.is_valid():
            lesson = form.save(commit=False)
            lesson.save()
            return redirect('lesson_detail', pk=lesson.pk)
    else:
        form = EditLessonForm(instance=lesson)
    return render(request, 'edit_lesson.html', {'form': form})


@permission_required('timetable.can_edit_lesson')
def active_lesson(request, pk):
    lesson = get_object_or_404(Lesson, pk=pk)
    students = lesson.group.get_students()
    studentnum = lesson.group.get_students().count()
    StudentActivitySet = formset_factory(StudentActivityForm, extra=studentnum)
    formset = StudentActivitySet(request.POST or None)
    for form,student in zip(formset,students):
        form.fields["person"].initial = student
        form.fields["person"].disabled = True
    if formset.is_valid():
        for form in formset:
            mark = form.save(commit=False)
            mark.lesson = lesson
            mark.save()
        return redirect('lesson_detail', pk=lesson.pk)

    return render(request, 'active_lesson.html', {'formset' : formset})


@login_required
def group_detail(request, pk):
    group = get_object_or_404(Group, pk=pk)
    students = group.get_students()

    return render(request, 'group_detail.html', context={'group': group, 'students': students})


@login_required
def export_activity_to_xlsx(request, pk):
    user = get_object_or_404(User, pk=pk)
    profile = user.profile
    activity = Mark.objects.filter(person=profile)

    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    )

    response['Content-Disposition'] = 'attachment; filename={date}-activity.xlsx'.format(
        date=datetime.now().strftime('%Y-%m-%d'),
    )

    workbook = Workbook()

    worksheet = workbook.active
    worksheet.title = user.username + "_" + (profile.group.__str__())

    columns = [
        'Пара',
        'Дата',
        'Время',
        'Оценка',
        'Пропуск',
    ]
    row_num = 1

    # Assign the titles for each cell of the header
    for col_num, column_title in enumerate(columns, 1):
        cell = worksheet.cell(row=row_num, column=col_num)
        cell.value = column_title

    # Iterate through all movies
    for active in activity:
        row_num += 1

        # Define the data for each cell in the row
        if active.skip:
            skip_clean = "+"
        else:
            skip_clean = "-"
        row = [
            active.lesson.name,
            active.lesson.date,
            active.lesson.time_start,
            active.mark,
            skip_clean,
        ]

        # Assign the data for each cell of the row
        for col_num, cell_value in enumerate(row, 1):
            cell = worksheet.cell(row=row_num, column=col_num)
            cell.value = cell_value

    workbook.save(response)

    return response
